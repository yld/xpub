CSS variables are used to define font families.

`--font-author`

```js
<div style={{ fontFamily: 'var(--font-author)' }}>
{faker.lorem.sentence(5)}
</div>
```

`--font-reviewer`

```js
<div style={{ fontFamily: 'var(--font-reviewer)' }}>
{faker.lorem.sentence(5)}
</div>
```

`--font-interface`

```js
<div style={{ fontFamily: 'var(--font-interface)' }}>
{faker.lorem.sentence(5)}
</div>
```

`--font-mono`

```js
<div style={{ fontFamily: 'var(--font-mono)' }}>
{faker.lorem.sentence(5)}
</div>
```
