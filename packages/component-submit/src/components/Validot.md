A dot representing the validation state of a form field.

```js
<Validot meta={{ valid: false, error: 'There was an error' }}/>
```

```js
<Validot meta={{ valid: true, warning: 'There was a warning' }}/>
```

```js
<Validot meta={{ valid: true }}/>
```
