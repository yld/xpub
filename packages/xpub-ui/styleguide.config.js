module.exports = {
  title: 'xpub-ui style guide',
  styleguideComponents: {
    StyleGuideRenderer: require.resolve('xpub-styleguide/src/components/StyleGuideRenderer'),
    Wrapper: require.resolve('xpub-styleguide/src/components/Wrapper')
  },
  context: {
    faker: 'faker',
  },
  skipComponentsWithoutExample: true,
  theme: {
    fontFamily: {
      base: '"Fira Sans", sans-serif'
    },
    color: {
      link: 'cornflowerblue'
    }
  },
  sections: [
    {
      name: 'Colors',
      content: 'docs/colors.md'
    },
    {
      name: 'Fonts',
      content: 'docs/fonts.md'
    },
    {
      name: 'Atoms',
      components: 'src/atoms/*.js'
    },
    {
      name: 'Molecules',
      components: 'src/molecules/*.js'
    }
  ]
}
