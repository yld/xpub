export default {
  author: 'Author',
  seniorEditor: 'Senior Editor',
  managingEditor: 'Managing Editor',
  handlingEditor: 'Handling Editor',
}
