An editor for plain text, with no options for adding blocks or applying text formatting.

```js
<TextEditor
    value=""
    placeholder="Enter some text…"
    onChange={value => console.log(value)}/>
```
