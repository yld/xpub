A button.

```js

<Button>Save</Button>
```

A button can be disabled.

```js
<Button disabled>Save</Button>
```

A button can be marked as the "primary" action.

```js
<Button primary>Save</Button>
```
