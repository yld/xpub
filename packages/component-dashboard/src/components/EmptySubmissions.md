A message that is displayed when the current user has no submissions.

```js
<EmptySubmissions/>
```
