// define the Slate type (mark) for each keyboard shortcut

export default {
  b: 'bold',
  i: 'italic'
}
